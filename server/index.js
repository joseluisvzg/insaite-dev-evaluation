var express = require('express');
var app = express();
const https = require('https');
var cors = require('cors');

app.use(cors());
// To add: Add specific domain to CORS

app.get("/random_numbers", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    
    https.get('https://1er1go3qtc.execute-api.us-east-1.amazonaws.com/dev/evaluation-dev', (resp) => {
    let data = '';
    resp.on('data', (chunk) => {
        data += chunk;
    });
    resp.on('end', () => {
        res.send(data);
    });
    }).on("error", (err) => {
        // To add: Add 500 error within header
        res.send({"message":"error"})
    });
});

app.listen(5000, () => {
    console.log('Express app listening on port 5000')
});