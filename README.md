Creo que el código relacionado a la transformación resultaría más eficiente en el front-end ya 
que los números vienen de manera aleatoria desde el servicio. En caso de que estos datos (números)
 no cambien de forma regular, entonces se podrían ordenar en el servidor y aplicar alguna técnica de cache para reducir los tiempos de procesamiento y devolverlos ya transformados.

